class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :country
      t.string :fio
      t.string :lastname
      t.string :firstname
      t.string :creditbook
      t.date :creditbooksigned
      t.string :phone
      t.boolean :allowed
t.references :user, index: true, foreign_key: true
      t.timestamps
    end
add_index :students, [:user_id, :created_at]
  end
end
