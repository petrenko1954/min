#Последний Листинг :8.1 :8.49: Обработка флажка “remember me”. app/controllers/#sessions_controller.rb  
#Листинг 3.37 :11.29 :12.20
Rails.application.routes.draw do
   resources :microposts
  

root 'static_pages#home'
  get 'sessions/new'

  get 'password_resets/new'

  get 'password_resets/edit'

resources :users
  resources :students

 #/home/sf/libr/config      Aug30   
#root 'users#index'
#/Aug30
 
#get 'articles/index'
get 'articles' => 'articles#index'

#get 'static_pages/home'
 # get 'static_pages/help'
  #get 'static_pages/about'
  #get 'static_pages/users'
  #get 'static_pages/referatHartl'
  get 'static_pages/usersallowed'
 get "/static_pages/users"
  # Sept01  get 'sessions/new'
#Листинг 5.22: Маршруты для статических страниц. config/routes.rb Листинг 7.3:
 #Articles Articles_path
#get 'Articles'    => '/articles'
get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
get 'signup'  => 'users#new'  #Листинг 5.33:Маршрут для страницы регистрации. config/#routes.rb Листинг 7.3:
                  #get 'users'  => 'static_pages#users'

# sept 10 get 'sessions/new'
#Листинг 10.1: Добавление ресурса для активаций аккаунта. config/routes.rb :11.29
resources :account_activations, only: [:edit]
resources :password_resets,     only: [:new, :create, :edit, :update]
resources :microposts,          only: [:create, :destroy]
   resources :relationships,       only: [:create, :destroy]

resources :articles
  resources :folders
  resources :reports
#Листинг 8.1
 get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
#12.15
resources :users do 
    member do
      get :following, :followers
    end
  end



#Листинг 5.33: Маршрут для страницы регистрации. config/routes.rb 
 # ! devise_for :users
 # ! devise_for :admin_users, ActiveAdmin::Devise.config
 #ActiveAdmin.routes(self)
  #  root 'users#show' был  у Мелаш
  # Example of regular route:
  get 'attach_files' => 'migration#attach_files'
  post 'attach_files' => 'migration#attach_files_action'

  post 'api/reviews_by_chapter' => 'categories#reviews_by_chapter'
  post 'api/sections_by_review' => 'categories#sections_by_review'
  post 'api/subsections_by_section' => 'categories#subsections_by_section'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
 

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable


  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
