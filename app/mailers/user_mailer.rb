#Листинг 10.9: 10.11 Созданный User mailer. app/mailers/user_mailer.rb 
  # Тема письма может быть указана в файле I18n config/locales/en.yml
  # следующим образом:
  #
  #   en.user_mailer.account_activation.subject
  #
 class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account activation"
  end

  def password_reset
    @greeting = "Hi"

    mail to: "to@example.org"
  end
end

