json.array!(@students) do |student|
  json.extract! student, :id, :country, :fio, :lastname, :firstname, :creditbook, :creditbooksigned, :phone, :allowed
  json.url student_url(student, format: :json)
end
