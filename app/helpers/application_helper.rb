#Листинг 4.2: Определение хелпера full_title. app/helpers/application_helper.rb

module ApplicationHelper

  # Возвращает полный заголовок на основе заголовка страницы.
  def full_title(page_title = '')
    base_title = "min1"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end

