#Листинг 11.38: Добавление переменной экземпляра @micropost в действие home. app/controllers/static_pages_controller.rb :11.45 :12.48
class StaticPagesController < ApplicationController
  def home
if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
end
  end

  def help
  end

  def about
  end

  def referat
  end

  def students
  end

  def something
  end
end
