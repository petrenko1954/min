#Листинг 8.11: Внесение модуля Sessions helper в контроллер Application. app/controllers/application_controller.rb :11.31

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

               private

    # Проверяет статус входа пользователя.
   def login(user)
    session[:user_id] = user.id
end  

def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
end


