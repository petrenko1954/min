#Листинг :7.5 :7.12 :7.17 :7.18 :7.23 :8.22: Вход пользователя после регистрации. app/controllers/users_controller.rb !! :9.12 9.28: :9.32  :9.33
#Листинг 9.1: Действие edit. app/controllers/users_controller.rb :9.53 :11.22 :9.53 :12.25

class UsersController < ApplicationController
before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
                                        :following, :followers]

  before_action :correct_user,   only: [:edit, :update]
before_action :admin_user, only: :destroy

def index
@users = User.all
   @users = User.paginate(page: params[:page])      
  end

def show
    @user = User.find(params[:id])
@microposts = @user.microposts.paginate(page: params[:page])
 @student = Student.find(params[:id])
  end
# :9.25
#before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user,   only: [:edit, :update]
  def edit
    @user = User.find(params[:id])
  end

def update
@user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def new
    @user = User.new
  end
#Листинг 10.21: Добавление активации аккаунта к регистрации пользователя. КРАСНЫЙ app/controllers/users_controller.rb 
# :10.34 Sept 26Листинг 11.22: Добавление переменной экземпляра @microposts к действию show. app/controllers/users_controller.r
 #8.22
def create
   @user = User.new(user_params)
      if @user.save
         log_in @user

          @user.send_activation_email

           flash[:info] = "Sept26 Please check your email to activate your account."
           redirect_to root_url

          
           #flash[:success] = "Welcome to the Sample App!"
           #redirect_to @user
         else
           render 'new'
    end
  end

def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                  :password_confirmation)
    end
# Предфильтры

    # Подтверждает вход пользователяВспомним из Раздела 9.2.1, что требование входа обеспечивает предфильтр с методом logged_in_user (Листинг 9.12). В то время он был нужен только в контроллере Users, но теперь в нем появилась необходимость и в контроллере Microposts, поэтому мы переместим его в контроллер Application, который является базовым классом для всех контроллеров (Раздел 4.4.4). Результат показан в Листинге 11.31.
#11.31 Удаляем
#11.31    def logged_in_user
 #11.31     unless logged_in?
 #11.31          store_location
 #11.31       flash[:danger] = "Please log in."
 #11.31       redirect_to login_url
 #11.31     end


# Подтверждает администратора.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
 
    # Подтверждает правильного пользователя
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end


