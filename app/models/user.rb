#Листинг :6.39 8.31 8.33: Добавление метода authenticated? к модели User. app/models/user.rb #Листинг 8.18: :8.32 :8.38 :9.10 :10.3
#Листинг 10.33: Добавление методов активации пользователя в модель User. app/models/user.rb  :11.10 :11.18 :12.8 :12.10, :12.12 :10.33 :12.43 :12.46

class User < ActiveRecord::Base
#11.10 :11.18 Листинг 11.44: Предварительная реализация потока микросообщений. app/models/user.rb 
  has_many :microposts, dependent: :destroy
   has_many :students, dependent: :destroy

#12.12
has_many :active_relationships,  class_name:  "Relationship",
                                   foreign_key: "follower_id",
                                   dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
has_many :following, through: :active_relationships, source: :followed
has_many :followers, through: :passive_relationships, source: :follower
# :10.3
 attr_accessor :remember_token, :activation_token
  before_save   :downcase_email
  before_create :create_activation_digest
  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
    validates :password, presence: true, length: { minimum: 6 }, allow_nil: true


  # Возвращает дайджест данной строки
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
# Возвращает случайный токен
  def User.new_token
    SecureRandom.urlsafe_base64
  end
 # :8.32 Запоминает пользователя в базе данных для использования в постоянной сессии.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
# Возвращает true, если предоставленный токен совпадает с дайджестом.
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
#Листинг 8.31: Добавление метода для генерации токена. app/models/user.rb 
#Листинг 8.33: Добавление метода authenticated? к модели User. app/models/user.rb 
def authenticated?(remember_token)
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end
#Листинг 8.38: Добавление метода forget к модели User. app/models/user.rb 

# Забывает пользователя
def forget
    update_attribute(:remember_digest, nil)
  end
# :10.3 :10.33

  # Активирует аккаунт.
def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Отправляет электронное письмо для активации. user.rb :10.33
  def send_activation_email
#UserMailer.account_activation(self).deliver_now    
UserMailer.account_activation(self).deliver
  end
# Определяет прото-ленту.
  # Полная реализация в "Следовании за пользователями".
 #def feed
 #   Micropost.where("user_id = ?", id)
 # end 
# Начать читать сообщения пользователя.
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Перестать читать сообщения пользователя.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end
 # Возвращает true, если текущий пользователь читает сообщения другого пользователя.
  def following?(other_user)
    following.include?(other_user)
  end
 # Возвращает true, если истек срок годности сброса пароля.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Возвращает ленту сообщений пользователя.
  def feed
   following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)

  end

  # Начинает читать сообщения пользователя..
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

                                         private

    # Переводит адрес электронной почты в нижний регистр.
    def downcase_email
      self.email = email.downcase
    end

    # Создает и присваивает активационнй токен и дайджест.
    def create_activation_digest
      self.activation_token  = User.new_token
      #self.activation_digest = User.digest(activation_token)
    end
end

